package com.br.testes;

public class Person {
	/*
	 * private int value;
	 * 
	 * public String hello() { return "hello"; }
	 * 
	 * public Person(int value) { super(); this.value = value; }
	 * 
	 * public boolean isEven() { if(value % 2== 0) return true; return false; }
	 * 
	 * public boolean isOdd() { if (!isEven()) return true; return false; }
	 
	 *public boolean isOdd() { if (!isEven()) return true; return false; }
	 */
	public static String ola(String comprimento) {
		String comp = comprimento;
		return comp;
	}

	public static String olaNo(String comprimento) {
		String comp = comprimento;
		return comp;
	}

	public static float ExecutaCalculo(float Valor1, float Valor2) {
		float Soma = Valor1 + Valor2;
		return Soma;
	}

	public static float ExecutaCalculoSubtracaoNo(float Valor1, float Valor2) {
		float Soma = Valor1 - Valor2;
		return Soma;
	}

	public static float ExecutaCalculoSubtracaoOk(float Valor1, float Valor2) {
		float Soma = Valor1 - Valor2;
		return Soma;
	}

	public static int ExecutaCalculos(int Valor1, int Valor2, int Valor3, int Valor4) {
		int soma = Valor1 + Valor2;
		int multiplica = soma + Valor3 * Valor4;
		int result = multiplica;
		return result;
	}

	public static int ExecutaCalculosNo(int Valor1, int Valor2, int Valor3, int Valor4) {
		int soma = Valor1 + Valor2;
		int multiplica = soma + Valor3 * Valor4;
		int result = multiplica;
		return result;
	}

	public static String palavrasUnidas(String palavra1, String palavra2, String palavra3, String palavra4,
			String palavra5) {
		String juncao = palavra1.concat(palavra2).concat(palavra3).concat(palavra4).concat(palavra5);
		return juncao;
	}

	public static String palavrasUnidas2(String palavra1, String palavra2, String palavra3, String palavra4,
			String palavra5) {
		String juncao = palavra1.concat(palavra2).concat(palavra3).concat(palavra4).concat(palavra5);
		return juncao;
	}
	
	public static String palavrasInvertidas(String palavra1, String palavra2, String palavra3) {
		String juncao = palavra1.concat(palavra2).concat(palavra3);
		
		return new StringBuilder(juncao).reverse().toString();
	}
	
	public static String palavrasInvertidas2(String palavra1, String palavra2, String palavra3) {
		String juncao = palavra1.concat(palavra2).concat(palavra3);
		
		return new StringBuilder(juncao).reverse().toString();
	}
}
