package com.br.testes;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;

public class PersonTeste extends TestCase {
	/*
	 * @Test void test() { Person person = new Person(0); assertEquals("hello",
	 * person.hello()); }
	 */
	public void testOla() {
		String PassaValor = "Teste executado com sucesso";
		String RetornoEsperado = "Teste executado com sucesso";
		String RetornoFeito = Person.ola(PassaValor);
		assertEquals(RetornoEsperado, RetornoFeito);

	}

	public void testOlaNo() {
		String PassaValor = "Teste";
		String RetornoEsperado = "Teste executado com sucesso";
		String RetornoFeito = Person.olaNo(PassaValor);
		assertEquals(RetornoEsperado, RetornoFeito);

	}

	public void testExecutaCalculo() {
		float PassaValor1 = 10;
		float PassaValor2 = 5;
		float RetornoEsperado = 15;
		float RetornoFeito = Person.ExecutaCalculo(PassaValor1, PassaValor2);
		assertEquals(RetornoEsperado, RetornoFeito, 0);
	}

	public void testExecutaCalculoSubtracaoNo() {
		float PassaValor1 = 10;
		float PassaValor2 = 5;
		float RetornoEsperado = 3;
		float RetornoFeito = Person.ExecutaCalculoSubtracaoNo(PassaValor1, PassaValor2);
		assertEquals(RetornoEsperado, RetornoFeito, 0);
	}

	public void testExecutaCalculoSubtracaoOk() {
		float PassaValor1 = 10;
		float PassaValor2 = 5;
		float RetornoEsperado = 5;
		float RetornoFeito = Person.ExecutaCalculoSubtracaoOk(PassaValor1, PassaValor2);
		assertEquals(RetornoEsperado, RetornoFeito, 0);
	}

	public void testExecutaCalculos() {
		int PassaValor1 = 2;
		int PassaValor2 = 2;
		int PassaValor3 = 2;
		int PassaValor4 = 3;
		int RetornoEsperado = 10;
		int RetornoFeito = Person.ExecutaCalculos(PassaValor1, PassaValor2, PassaValor3, PassaValor4);
		assertEquals(RetornoEsperado, RetornoFeito, 0);
	}

	public void testExecutaCalculosNo() {
		int PassaValor1 = 2;
		int PassaValor2 = 2;
		int PassaValor3 = 2;
		int PassaValor4 = 3;
		int RetornoEsperado = 8;
		int RetornoFeito = Person.ExecutaCalculosNo(PassaValor1, PassaValor2, PassaValor3, PassaValor4);
		assertEquals(RetornoEsperado, RetornoFeito, 0);
	}

	public void testpalavrasUnidas() {
		String PassaValor1 = "janela";
		String PassaValor2 = "escada";
		String PassaValor3 = "mesa";
		String PassaValor4 = "lampada";
		String PassaValor5 = "cadeira";

		String RetornoEsperado = "janelaescadamesalampadacadeira";
		String RetornoFeito = Person.palavrasUnidas(PassaValor1, PassaValor2, PassaValor3, PassaValor4, PassaValor5);
		assertEquals(RetornoEsperado, RetornoFeito);

	}

	public void testpalavrasUnidas2() {
		String PassaValor1 = "janela";
		String PassaValor2 = "janela";
		String PassaValor3 = "janela";
		String PassaValor4 = "lampada";
		String PassaValor5 = "cadeira";

		String RetornoEsperado = "janelajanelamesalampadacadeira";
		String RetornoFeito = Person.palavrasUnidas2(PassaValor1, PassaValor2, PassaValor3, PassaValor4, PassaValor5);
		assertEquals(RetornoEsperado, RetornoFeito);

	}
	
	public void testpalavrasInvertidas() {
		String PassaValor1 = "janela";
		String PassaValor2 = "janela";
		String PassaValor3 = "janela";

		String RetornoEsperado = "alenajalenajalenaj";
		String RetornoFeito = Person.palavrasInvertidas(PassaValor1, PassaValor2, PassaValor3);
		assertEquals(RetornoEsperado, RetornoFeito);

	}
	
	public void testpalavrasInvertidas2() {
		String PassaValor1 = "alenaj";
		String PassaValor2 = "alenaj";
		String PassaValor3 = "alenaj";

		String RetornoEsperado = "alenajalenajalenaj";
		String RetornoFeito = Person.palavrasInvertidas(PassaValor1, PassaValor2, PassaValor3);
		assertEquals(RetornoEsperado, RetornoFeito);

	}
}
